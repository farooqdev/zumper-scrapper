require 'active_record'
require_relative './application_record'
require_relative './property'
require_relative './amenity'
# require 'Property'
# require 'Amenity'
require 'kimurai'
class CraigslistScrapper < Kimurai::Base

  @name = "winder_scraper"
  @engine = :selenium_chrome
  USER_AGENTS = ["Chrome", "Firefox", "Safari", "Opera"]
  @config = {
    disable_images: true,
    window_size: [1366, 768]
  }
  @start_urls = ["https://kpr.craigslist.org/search/apa?availabilityMode=0&postal=99362&sale_date=all%20dates&search_distance=7&sort=dist"]
  def parse(response, url, data: {})

    db_configuration_file = File.join(File.expand_path('../..', __FILE__), '..', 'config', 'database.yml')
    puts "======== #{db_configuration_file} ========"
    config = YAML.load(File.read(db_configuration_file))
    puts "======== #{config['development']} ========"
    connection = ActiveRecord::Base.establish_connection(config["development"])
    puts "======== #{connection} ========"
    # Update response to current response after interaction with a browser
    response = browser.current_response

    details_list = response.xpath("//li[@class='result-row']")
    detail_count = details_list.count

    

    # base_url = "https://windermerepropmgmt.appfolio.com"

    details_list.each do |list|

      url = list.xpath("a")&.attr("href").value

      request_to :parse_property_data, url: url, data: {}

    end
   
  end

  def parse_property_data(response, url:, data: {})
    begin

      response = browser.current_response

      sleep 4

      beds = response.xpath("//span[@class='shared-line-bubble'][1]/b[1]")&.text
      bathroom = response.xpath("//span[@class='shared-line-bubble'][1]/b[2]")&.text
      sqft = response.xpath("//p[@class='attrgroup'][1]/span[@class='shared-line-bubble'][2]")&.text
      price = response.xpath("//span[@class='postingtitletext']/span[@class='price']")&.text 
      description = response.xpath("//section[@id='postingbody']")&.text
      address_line1 = response.xpath("//span[@class='postingtitletext']/span[@id='titletextonly']")&.text
      property = Property.create(bathroom: bathroom&.strip, address_line1: address_line1&.strip, rent: price&.strip , sqft: sqft&.strip ,beds: beds&.strip , full_description: description&.strip)

      amenities = response.xpath("//section[@id='postingbody']/ul[2]/li")
      amenities.each do |amenity|
         property&.amenities.create( name: amenity&.text )
      end
    rescue Exception => e
      puts e.message
    end
  end
end

CraigslistScrapper.crawl!
