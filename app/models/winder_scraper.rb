require 'active_record'
require_relative './application_record'
require_relative './property'
require_relative './amenity'
# require 'Property'
# require 'Amenity'
require 'kimurai'
class WinderScraper < Kimurai::Base

  @name = "winder_scraper"
  @engine = :selenium_chrome
  USER_AGENTS = ["Chrome", "Firefox", "Safari", "Opera"]
  @config = {
    disable_images: true,
    window_size: [1366, 768]
  }
  @start_urls = ["https://windermerepropmgmt.appfolio.com/listings"]
  def parse(response, url, data: {})

    db_configuration_file = File.join(File.expand_path('../..', __FILE__), '..', 'config', 'database.yml')
    puts "======== #{db_configuration_file} ========"
    config = YAML.load(File.read(db_configuration_file))
    puts "======== #{config['development']} ========"
    connection = ActiveRecord::Base.establish_connection(config["development"])
    puts "======== #{connection} ========"
    # Update response to current response after interaction with a browser
    response = browser.current_response

    details_list = response.xpath("//a[@class='btn btn-secondary js-link-to-detail']")

    base_url = "https://windermerepropmgmt.appfolio.com"

    details_list.each do |list|

      url = list&.attr("href")

      request_to :parse_property_data, url: absolute_url(url, base: base_url), data: {}

    end
   
  end

  def parse_property_data(response, url:, data: {})
    begin

      response = browser.current_response

      sleep 4

      address_line1 = response.xpath("//h2[@class='font-weight-normal js-show-title']")&.text
      puts "==== address_line1: #{address_line1}"
      price = response.xpath("//h1[@class='font-weight-normal d-inline-block']")&.text
      puts "==== Price: #{price}"
      data_para = response.xpath("//p[@class='header__summary js-show-summary']")&.text
      puts "==== data_para: #{data_para}"

      
      restretch = data_para&.split("|")[0] if data_para.present?
      puts "==== restretch: #{restretch}"
      again_restretch = restretch&.split(",") if restretch.present?
      puts "==== again_restretch: #{again_restretch}"
      again_restretch.present? ? sqft = again_restretch[2].to_s + again_restretch[3].to_s : sqft = "N/A"
      puts "==== sqft: #{sqft}"
      again_restretch.present? ? beds = again_restretch[0] :  beds = "N/A"
      puts "==== beds: #{beds}"
      description =  response.xpath("//p[@class='listing-detail__description hand-hidden tablet-hidden font-weight-light']")&.text
      puts "==== description: #{description}"
     
          property = Property.create(address_line1: address_line1&.delete!("\n") , rent: price&.strip , sqft: sqft&.strip ,beds: beds&.strip , full_description: description&.strip)

          10.times do |t| 
              amenity =  response.xpath("//div[@class='grid__large-6 grid__medium-6 grid__small-12'][1]/ul[@class='list font-weight-light']/li[@class='list__item'][#{t+1}]")&.text
              puts "==== amenity: #{amenity}"
              if amenity.present?
                property&.amenities.create( name: amenity&.strip )
              end
          end

    rescue Exception => e
      puts e.message
    end
  end
end

WinderScraper.crawl!
