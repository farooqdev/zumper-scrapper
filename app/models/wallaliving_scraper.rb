require 'active_record'
require_relative './application_record'
require_relative './property'
require_relative './amenity'
require 'kimurai'

class WallalivingScraper < Kimurai::Base

  @name = "wallaliving_scraper"
  @engine = :selenium_chrome
  USER_AGENTS = ["Chrome", "Firefox", "Safari", "Opera"]
  @config = {
    disable_images: true,
    window_size: [1366, 768]
  }
  @start_urls = ["https://www.wallaliving.com/"]

  def parse(response, url, data: {})
    db_configuration_file = File.join(File.expand_path('../..', __FILE__), '..', 'config', 'database.yml')
    puts "======== #{db_configuration_file} ========"
    config = YAML.load(File.read(db_configuration_file))
    puts "======== #{config['development']} ========"
    connection = ActiveRecord::Base.establish_connection(config["development"])
    puts "======== #{connection} ========"
    # Update response to current response after interaction with a browser
    response = browser.current_response

    4.times do |t|
      begin
        t = t + 1
        address_line1 = response.xpath("//tr[#{t}]/td[1]/table/tbody/tr[1]/td[2]/b")&.text
        puts "==== address_line1: #{address_line1}"
        price = response.xpath("//tr[#{t}]/td[1]/table/tbody/tr[2]/td[2]")&.text
        puts "==== Price: #{price&.split("/")[0]}"
        beds = response.xpath("//tr[#{t}]/td[1]/table/tbody/tr[3]/td[2]")&.text
        puts "==== Beds: #{beds}"
        bathroom = response.xpath("//tr[#{t}]/td[1]/table/tbody/tr[4]/td[2]")&.text
        puts "==== Bathroom: #{bathroom}"
        description = response.xpath("//td[1]/table/tbody/tr[5]/td/b")&.text
        puts "==== description: #{description}"
        property = Property.create(address_line1: address_line1 , rent: price&.split("/")[0] ,beds: beds , full_description: description&.strip)
      rescue Exception => e
        puts e.message
      end
      
    end

  end

end

WallalivingScraper.crawl!
