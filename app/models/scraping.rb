require 'active_record'
require_relative './application_record'
require_relative './property'
require_relative './amenity'
# require 'Property'
# require 'Amenity'
require 'kimurai'
class Scraping < Kimurai::Base

  @name = "zumper_scraper"
  @engine = :selenium_chrome
  USER_AGENTS = ["Chrome", "Firefox", "Safari", "Opera"]
  @config = {
    # user_agent: "Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A334 Safari/7534.48.3",
    disable_images: true,
    window_size: [1366, 768]
    # restart_if: { memory_limit: 350_000 },
    # after_request: { delay: 1..5 },
    # headers: {user_agent: "Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A334 Safari/7534.48.3"}
    # headers: { "User-Agent": 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A334 Safari/7534.48.3'  }
  }
  @start_urls = ["https://www.zumper.com/apartments-for-rent/walla-walla-wa"]
  def parse(response, url, data: {})

    db_configuration_file = File.join(File.expand_path('../..', __FILE__), '..', 'config', 'database.yml')
    puts "======== #{db_configuration_file} ========"
    config = YAML.load(File.read(db_configuration_file))
    puts "======== #{config['development']} ========"
    connection = ActiveRecord::Base.establish_connection(config["development"])
    puts "======== #{connection} ========"
    # Update response to current response after interaction with a browser
    response = browser.current_response
    begin
      num = 0
      count = 0 
      stop_scrap = 0
      loop do
        response = browser.current_response
        num = num.to_i + 1     
        begin
          url = response.xpath("//div[@class='ListItem_ListItem__2erlV'][#{num}]/div[@class='ListItem_content__3hENK']/div[@class='ListItem_infoContainer__3Ki2B']/div[@class='ListItem_topInfoContainer__3huCo']/div[@class='AddressRow_AddressRow__3Idhx']/span[@class='AddressRow_header__2AqCH']/a[@class='AddressRow_headerText__3MY8y']").attr("href")&.value
          base_url = "https://www.zumper.com"
          clickable_div = browser.find(:xpath , "//div[#{num}]/div[@class='ListItem_content__3hENK']/div[@class='ListItem_infoContainer__3Ki2B']/div[@class='ListItem_topInfoContainer__3huCo']/div[@class='PriceRow_PriceRow__3U46J']/div[@class='PriceRow_price__2DaM9']", wait: 5)  
        rescue Exception => e
          puts e.message
          clickable_div = nil
        end
        puts "=====stop_scrap: #{stop_scrap} ==="
        if stop_scrap == 4
          break
        end 
        old_count = count 
        if clickable_div.present?
          stop_scrap = 0 
          count = count + 1
          request_to :parse_property_data1, url: absolute_url(url, base: base_url), data: {}
        else
          if old_count == count 
            stop_scrap = stop_scrap + 1
          end
          begin
            browser.execute_script("window.scrollBy(0,10000)") ; 
            sleep 10
            load_more_listing = browser.find(:xpath, "//button[@class='button_baseBtn__2y4lB button_zBtn__2zryS button_zBtnDefault__2Vb9d MoreListables_button__1yBm7']", wait: 5)
          rescue Exception => e
            puts e.message
            load_more_listing = nil
          end 
          if load_more_listing.present? 
            puts "click load more button: #{num}"
            load_more_listing.click 
            sleep 5
            num = num - 1
            response = browser.current_response
          else
            break
          end
        end       
      end
    rescue Exception => e
      puts e.message
    end
  end

    def parse_property_data1(response, url:, data: {})
    begin
      # last_height = browser.execute_script("return document.body.scrollHeight")
      last_height = 0
      loop do
        # Scroll down to bottom
        browser.execute_script("window.scrollBy(0, 768)")
        last_height = last_height + 768
        # Wait to load page
        sleep 4
        # Calculate new scroll height and compare with last scroll height
        new_height = browser.execute_script("return document.body.scrollHeight")
        puts "=========#{ new_height } ======"
        puts "=================last_height #{last_height} ========"
        if last_height >= new_height  
            break
        end
      end
      
      response = browser.current_response
      lori_sharp = response.xpath("//span[@class='AgentBox_name__zickq']")&.text
      puts "======= lori_sharp: #{lori_sharp} ======"
      if lori_sharp.downcase.include?  "lori sharp"
        beds = response.xpath("//div[@class='SummaryIcon_summaryIcon__2-kEF'][1]/div[@class='SummaryIcon_summaryText__2Su6m']")&.text
        puts "======Beds:  #{beds} ===== "
        bathrooms = response.xpath("//div[@class='SummaryIcon_summaryIcon__2-kEF'][2]/div[@class='SummaryIcon_summaryText__2Su6m']")&.text
        puts "======Bathrooms:  #{bathrooms} ===== "
        sqft = response.xpath("//div[@class='SummaryIcon_summaryIcon__2-kEF'][3]/div[@class='SummaryIcon_summaryText__2Su6m']")&.text
        puts "======sqft:  #{sqft} ===== "
        pets = response.xpath("//div[@class='SummaryIcon_summaryIcon__2-kEF'][4]/div[@class='SummaryIcon_summaryText__2Su6m']")&.text
        puts "======pets:  #{pets} ===== "
        time = response.xpath("//div[@class='SummaryIcon_summaryIcon__2-kEF'][5]/div[@class='SummaryIcon_summaryText__2Su6m']")&.text
        puts "======time:  #{time} ===== "
        address_line1 =  response.xpath("//h1[@class='H1_h1__A3Il4']")&.text
        puts "======address_line1:  #{address_line1} ===== "  
        description = response&.xpath("//div[@class='Description_description__1gLHl']/div[1]")&.text
        # description = browser.find(:xpath,"//div[@class='Description_description__1gLHl']/div[1]")&.text rescue "error"
        price = response.xpath("//div[@class='MessageSummary_priceText__3Sxbr']/div")&.text
        puts "======price:  #{price} ===== "
        unit_amenities = response&.xpath("//div[@class='FullAmenities_fullAmenities__LsJBD']/div[@class='Amenities_container__2et_S'][1]")[0]&.css(".Amenity_amenity__3LSDQ")
        puts "=== unit_amenities: #{unit_amenities} ===="
        building_amenities = response&.xpath("//div[@class='FullAmenities_fullAmenities__LsJBD']/div[@class='Amenities_container__2et_S'][2]")[0]&.css(".Amenity_amenity__3LSDQ")
        puts "=== building_amenities: #{building_amenities} ===="
          property = Property.create(address_line1: address_line1 , rent: price , sqft: sqft ,beds: beds ,pets: pets, full_description: description)
      
          unit_amenities&.each do |amenity|
            name = amenity.children.children.text
            amenity_type = "unit"
            property&.amenities.create(amenity_type: amenity_type , name: name )
          end
          count = 0
          building_amenities&.each do |amenity|
            name = amenity.children.children.text
            amenity_type = "building"
            property&.amenities.create(amenity_type: amenity_type , name: name )
          end
      end
      browser.go_back
    rescue Exception => e
      puts e.message
    end
  end
end

Scraping.crawl!
