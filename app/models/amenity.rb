# == Schema Information
#
# Table name: amenities
#
#  id           :integer          not null, primary key
#  name         :string
#  amenity_type :string
#  property_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Amenity < ActiveRecord::Base
	self.table_name = 'amenities'
	belongs_to :property
end
