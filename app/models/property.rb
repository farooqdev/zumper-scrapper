# == Schema Information
#
# Table name: properties
#
#  id               :integer          not null, primary key
#  address_line1    :string
#  rent             :string
#  sqft             :string
#  beds             :string
#  pets             :string
#  full_description :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Property < ActiveRecord::Base
	self.table_name = 'properties'
	has_many :amenities

end