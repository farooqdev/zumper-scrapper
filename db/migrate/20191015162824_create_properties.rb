class CreateProperties < ActiveRecord::Migration[5.1]
  def change
    create_table :properties do |t|
      
      t.string :address_line1
      t.string :rent
      t.string :sqft
      t.string :beds
      t.string :pets
      t.text :full_description

      t.timestamps
    end
  end
end
