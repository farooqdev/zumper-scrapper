class CreateAmenities < ActiveRecord::Migration[5.1]
  def change
    create_table :amenities do |t|

      t.string :name
      t.string :amenity_type
      t.references :property, foreign_key: true, index: true
      t.timestamps
    end
  end
end
